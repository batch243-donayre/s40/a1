const Course = require("../models/Courses");
const auth = require("../auth");


module.exports.addCourse = (request, response)=>{

	let token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	let newCourse = new Course ({
		name:request.body.name,
		description: request.body.description,
		price:request.body.price,
		slots: request.body.slots
	})
	if(userData.isAdmin){
		newCourse.save().then(result => {
				console.log(result)
				response.send(true)
			}).catch(error => {
				console.log(error);
				response.send(false);
			})
		}else{
		response.send("Sorry! you do not have an admin role");
		}
}

//Retrieve all active courses

module.exports.getAllActive = (request , response)=>{
	return Course.find({isActive: true}).then(result =>{
		response.send(result);
	}).catch(err =>{
		response.send(err);
	})
}


//retrieving specific course
module.exports.getCourse = (request, response) =>{
	const courseId = request.params.courseId;

	return Course.findById(courseId).then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

//update a course

module.exports.updateCourse =(request,response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	let updatedCourse = {
		name : request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
	}

	const courseId = request.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new: true}).then(result => {
			response.send(result);
		}).catch(err =>{
			response.send(err);
		})
	}else{
		return response.send("You don't have access to this page!");
	}
}


//Archiving Course
module.exports.archivingCourse =(request,response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	let courseArchive = {
		isActive : request.body.isActive,
	}

	const courseId = request.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, courseArchive, {new: true}).then(result => {
			response.send(true);
		}).catch(err =>{
			response.send(err);
		})
	}else{
		return response.send("Sorry! Only admin can access this page!");
	}
}


module.exports.getAllCourses = (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	// console.log(userData);

	if(!userData.isAdmin){
			return response.send("Sorry! You don't have access to this page!")
		}else{
			return Course.find({}).then(result => response.send(result))
			.catch(err =>{
			response.send(err);
		})
	}
}

