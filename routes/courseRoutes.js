const express = require ("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js");

router.post("/", auth.verify,courseControllers.addCourse);

//All params above

router.get("/allActiveCourses", courseControllers.getAllActive);

//Retrieve all course
router.get("/allCourses", auth.verify, courseControllers.getAllCourses);




//Specific course
router.get("/:courseId", courseControllers.getCourse);

//update specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

//Archiving course
router.patch("/:courseId/archive", auth.verify, courseControllers.archivingCourse);



module.exports = router;